# -*- coding: utf-8 -*-
import datetime
import os
import igraph
from genetic import ga_utilities

from genetic.genetic import *
from pygmalion.fitness_with_scale import *

fitness = Fitness(minimize=False)
#fitness.add((assortativity_fit, 1))
# fitness.add((ClusterisationFitnessCalculator(0.7), 3))
#fitness.add((community_fit, 1))
#fitness.add((mean_degree_fit, 1))
#fitness.add((modularity_fit, 1))
#fitness.add((sir_days_fit, 1))
fitness.add((components_fit, 1))
#fitness.add((eff_fit, 1))
#fitness.add((degree_sequence_fit, 1))


# fitness.add((DensityFitnessCalculator(0.05), 2))

fitness.add((SirFitnessCalculator(0.65), 1))
#fitness.add((motif_fit, 1))
# fitness.add((pl_fit, 1))


def run_program():
    save_directory_name = 'simulations/ga_' + datetime.datetime.today().strftime("%d.%m.%y %H:%M:%S") + '/'
    os.mkdir(save_directory_name)
    n = 100

    genetics = DismantleGraph(n, limit=1000, size=100,
                              prob_crossover=0.6,
                              prob_mutation=0.8, fit=fitness, verbose=True)

    ga = GeneticAlgorithm(genetics)

    try:
        genetic_best = ga.search_run()
        print 'Best strategy: ' + str(genetic_best)
    except KeyboardInterrupt:
        pass
    finally:
        print 'Fitness calls N: ', ga.genetics.fit.calls
        exporter = ga_utilities.Exporter(save_directory_name)
        exporter.export_all(ga)


if __name__ == '__main__':
    run_program()
