# -*- coding: utf-8 -*-
from collections import Counter, defaultdict
import itertools
import os
import datetime
import random
import igraph
import igraphplus.metrics
from matplotlib import pyplot as plt
from matplotlib import rc
import math
import warnings
from scipy.interpolate import interp1d

font = {'family': 'Arial',
        'weight': 'normal',
        'size': 14}
rc('font', **font)

import numpy as np


def make_graph_pool(n):
    #graph_pool = [igraph.Graph(n)]
    #max_e = n * (n - 1) / 2
    #step = max_e / 10
    #while step < max_e:
    #    graph_pool.append(igraph.Graph.Erdos_Renyi(n, m=step))
    #    step += step

    graph_pool = [igraph.Graph.Erdos_Renyi(n, m=random.randint(0, n * (n - 1) / 2)) for __ in range(20)]
    return graph_pool


def smooth(x, window_len=11, window='hanning'):
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"

    s = np.r_[x[window_len - 1:0:-1], x, x[-1:-window_len:-1]]
    #print(len(s))
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')
    return y


def generate_initial_g(fitness, number_of_nodes=500):
    g = None
    pool = make_graph_pool(number_of_nodes)
    min_fit = 100
    for p in pool:
        p_fit, dic, values = fitness.calculate_fitness(p)
        if p_fit < min_fit:
            min_fit = p_fit
            g = p
    return g


class Animation(object):
    def __init__(self, alg):
        self.alg = alg
        self.system = alg.system
        self.system.g.vs['size'] = 3
        self.layout = self.system.g.layout('fr')
        self.g_dic = dict()

    def add_g(self):
        self.g_dic[self.alg.current_iteration] = self.system.g

    def make_slide(self):
        igraph.plot(self.system.g,
                    'results/visualization/%d.png' % self.alg.current_iteration,
                    vertex_color='#00000088',
                    edge_color='#22222233',
                    edge_width=1.5,
                    layout=self.layout,
                    bbox=(0, 0, 500, 500))

    def make_slideshow(self):
        save_directory_name = 'results/visualization/' + datetime.datetime.today().strftime("%d.%m.%y.%H.%M.%S") + '/'
        os.mkdir(save_directory_name)
        layout = self.system.g.layout('fr')
        width = 800
        density = math.ceil(width / self.g_dic.values()[-1].vcount())
        for i, v in self.g_dic.items():
            v.vs['size'] = [2 * math.ceil(math.log(1 + x, 2)) + density for x in v.degree()]
            graph_filename = save_directory_name + '%s.png' % str(i).zfill(10)
            igraph.plot(v, graph_filename, vertex_color='#00000099',
                        edge_color='#22222299', edge_width=1.5, layout=layout, bbox=(0, 0, width, width))

            plt.clf()
            plt.xlim(0, len(self.alg.log_keeper['energy']))
            plt.ylim(0, self.alg.log_keeper['energy'][0] + 0.05)
            plt.ylabel(u'Энергия системы')
            plt.xlabel(u'Итерация')
            plt.plot(self.alg.log_keeper['energy'][0:i], c='k')
            plot_filename = save_directory_name + '%s_e.png' % str(i).zfill(10)
            plt.savefig(plot_filename, bbox_inches='tight', dpi=200)
            os.system('convert %s -resize \'x%d\' %s' % (plot_filename, width, plot_filename))
            os.system('convert +append %s %s results/vis/%s.png' % (graph_filename, plot_filename, str(i).zfill(10)))


class ProcessVisualization(object):
    def __init__(self, system):
        self.system = system
        self.system.g.vs['size'] = 3
        self.layout = system.g.layout('fr')
        plt.ion()

    def visualize(self, alg, fin=True):
        if self.system.current_iteration % 200 != 0:
            return
        plt.subplot(211)
        plt.title(u'Схема охлаждения')
        plt.xlim(0, len(alg.decisions))
        plt.plot(alg.decisions, 'k*')
        plt.subplot(212)
        plt.title(u'Энергия')
        plt.xlim(0, len(alg.energy))
        plt.plot(alg.energy, c='k')
        if not fin:
            plt.draw()
        else:
            plt.show()


class Exporter(object):
    def __init__(self, save_directory):
        self.save_directory = save_directory

    def export_energy_and_prob_profile(self, alg):
        f = open(self.save_directory + 'probability_energy_log.txt', 'w')
        f.write('prob\tenergy\n')
        for k in range(len(alg.log_keeper['decisions'])):
            f.write(str(alg.log_keeper['decisions'][k]) + '\t' + str(alg.log_keeper['energy'][k]) + '\n')
        f.close()

    def visualize_algorithm_data_summary(self, alg):
        plt.clf()
        plt.subplot(311)
        plt.title(u'Схема охлаждения')
        plt.xlim(0, len(alg.decisions))
        plt.ylabel(r'$P(t, E)$')
        plt.xlabel(u'Итерация')
        plt.plot(alg.decisions, 'k*')
        plt.subplot(312)
        plt.title(u'Энергия системы')
        plt.xlim(0, len(alg.energy))
        plt.ylabel(u'E')
        plt.xlabel(u'Итерация')
        plt.plot(alg.energy, c='k')
        plt.subplot(313)
        plt.title(u'Число ребер')
        plt.xlim(0, len(alg.log_keeper['edge_count']))
        plt.tight_layout()
        plt.xlabel(u'Итерация')
        plt.plot(alg.log_keeper['edge_count'], c='k')
        plt.savefig(self.save_directory + 'result_data_summary.png')
        plt.show()

    def visualize_algorithm_data_separate(self, alg):
        plt.clf()
        plt.title(u'Схема охлаждения')
        plt.xlim(0, len(alg.log_keeper['decisions']))
        plt.ylabel(r'$P(t, E)$')
        plt.xlabel(u'Итерация')
        plt.plot(alg.log_keeper['decisions'], 'k*')
        plt.savefig(self.save_directory + 'result_temperature_log.png')

        plt.clf()
        plt.title(u'Энергия системы')
        plt.xlim(0, len(alg.log_keeper['energy']))
        plt.ylabel(u'E')
        plt.xlabel(u'Итерация')
        plt.plot(alg.log_keeper['energy'], c='k')
        plt.savefig(self.save_directory + 'result_energy_log.png')
        plt.clf()

        plt.title(u'Число ребер')
        plt.xlim(0, len(alg.log_keeper['edge_count']))
        plt.tight_layout()
        plt.xlabel(u'Итерация')
        plt.ylabel(u'|E|')
        plt.plot(alg.log_keeper['edge_count'], c='k')
        plt.savefig(self.save_directory + 'result_edge_count.png')

    @staticmethod
    def visualize_correlation(alg):
        hist = alg.system.fit_hist
        pairs = Exporter.get_highly_correlated_pairs(hist)
        s = len(pairs)
        print s
        if s == 0:
            return
        for index, pair in enumerate(pairs):
            plt.subplot(s, 1, index + 1)
            plt.xlabel(pair[0])
            plt.ylabel(pair[1])
            plt.title(str(pair[2]))
            plt.scatter(hist[pair[0]], hist[pair[1]], s=10, alpha=0.5)
        plt.tight_layout()
        plt.show()

    @staticmethod
    def export_correlation(alg):
        hist = alg.system.fit_hist
        pairs = Exporter.get_highly_correlated_pairs(hist)
        s = len(pairs)
        print s
        for index, pair in enumerate(pairs):
            p1 = pair[0]
            p2 = pair[1]
            #plt.title(str(pair[2]))
            #plt.scatter(hist[pair[0]], hist[pair[1]], s=10, alpha=0.5)
            #plt.tight_layout()
            #plt.show()

    @staticmethod
    def visualize_actions_success(alg):
        hist = alg.log_keeper['action']
        for index, accept in enumerate(alg.log_keeper['accepted']):
            if accept == 0:
                hist[index] = -1
        step = 50
        f, p = divmod(len(hist), step)
        hist = hist[:-p]
        data = np.array(hist).reshape(len(hist) // step, step)
        values = defaultdict(lambda: list())
        for section in data:
            count = Counter(section)
            values['0'].append(count[0])
            values['1'].append(count[1])
            values['2'].append(count[2])

        plt.clf()
        plt.boxplot([values['0'], values['1'], values['2']])
        plt.show()

    def visualize_actions(self, alg):
        hist = alg.log_keeper['action']
        for index, accept in enumerate(alg.log_keeper['accepted']):
            if accept == 0:
                hist[index] = -1
        step = alg.current_iteration // 10
        #step = 50
        f, p = divmod(len(hist), step)
        hist = hist[:-p]
        data = np.array(hist).reshape(len(hist) // step, step)
        values = defaultdict(lambda: list())
        for section in data:
            count = Counter(section)
            values['0'].append(count[0])
            values['1'].append(count[1])
            values['2'].append(count[2])

        plt.clf()
        #fig = plt.figure()
        x = map(lambda z: z * step, range(1, f + 1))
        ff0 = interp1d(x, values['0'], kind='cubic')
        ff1 = interp1d(x, values['1'], kind='cubic')
        ff2 = interp1d(x, values['2'], kind='cubic')
        xx = np.linspace(x[0], x[-1], step // 5)
        plt.plot(xx, ff0(xx), color='k', marker='o', label=u'Добавление')
        plt.plot(xx, ff1(xx), color='k', marker='d', label=u'Удаление')
        plt.plot(xx, ff2(xx), color='k', marker='H', label=u'Перевязка')
        #plt.plot(x, values['0'], marker='o', label=u'Добавление')
        #plt.plot(x, values['1'], marker='o', label=u'Удаление')
        #plt.plot(x, values['2'], marker='o', label=u'Перевязка')
        plt.xlabel(u'Итерация')
        plt.ylabel(u'Число удачных мутаций')
        plt.grid(color='gray', linestyle='-', linewidth=0.5, alpha=0.5)
        plt.legend(loc='best')

        plt.tight_layout()
        #self.set_fig_lines_bw(fig)
        plt.savefig(self.save_directory + 'actions_log.png')
        plt.show()

    def visualize_actions_smooth(self, alg):
        hist = alg.log_keeper['action']
        for index, accept in enumerate(alg.log_keeper['accepted']):
            if accept == 0:
                hist[index] = -1
        step = alg.current_iteration // 20
        f, p = divmod(len(hist), step)
        hist = hist[:-p]
        data = np.array(hist).reshape(len(hist) // step, step)
        values = defaultdict(lambda: list())
        for section in data:
            count = Counter(section)
            values['0'].append(count[0])
            values['1'].append(count[1])
            values['2'].append(count[2])

        plt.clf()
        x = map(lambda z: z * step, range(1, f + 1))

        plt.plot(smooth(np.array(values['0']), window_len=5, window='blackman'), marker='o', label=u'Добавление')
        plt.plot(smooth(np.array(values['1']), window_len=5, window='blackman'), marker='o', label=u'Удаление')
        plt.plot(smooth(np.array(values['2']), window_len=5, window='blackman'), marker='o', label=u'Перевязка')

        plt.xlabel(u'Итерация')
        plt.ylabel(u'Число удачных мутаций')
        plt.grid(color='gray', linestyle='-', linewidth=0.5, alpha=0.5)
        plt.legend(loc='best')

        plt.tight_layout()
        plt.show()


    @staticmethod
    def visualize_fitnesses(alg):
        hist = alg.log_keeper['fitness']
        plt.clf()
        for k, v in hist.items():
            plt.plot(v, marker='o', label=k, alpha=0.5)

        plt.grid(color='gray', linestyle='-', linewidth=0.5, alpha=0.5)
        plt.legend(loc='best')
        plt.tight_layout()
        plt.show()

    def visualize_fitnesses_with_smoothing(self, alg):
        markers = itertools.cycle(['H', 'd', 'o', ',', 'D', 'p', 'v'])
        hist = alg.log_keeper['fitness']
        plt.clf()
        for k, v in hist.items():
            a = np.array(v)
            yy = smooth(a, window_len=100, window='blackman')
            x, y = [], []
            for i, v in enumerate(yy):
                if i % 250 == 0:
                    x.append(i)
                    y.append(v)

            plt.plot(x, y, color='k', marker=next(markers), label=k, linewidth=1.5, alpha=0.8)

        plt.grid(color='gray', linestyle='-', linewidth=0.5, alpha=0.5)
        plt.legend(loc='best')
        plt.tight_layout()
        plt.savefig(self.save_directory + 'fitness_log.png')
        plt.show()

    def visualize_graph_structure(self, g, name='resulting_graph'):
        width = 1000
        density = math.ceil(width / g.vcount())
        layout = g.layout('fr')
        g.vs['size'] = [2 * math.ceil(math.log(1 + x, 2)) + density for x in g.degree()]
        igraph.plot(g, self.save_directory + name + '.png', vertex_color='#000000',
                    edge_color='#11111199', edge_width=2, layout=layout, bbox=(0, 0, width, width))

    def visualize_graph_structure_types(self, g, name='resulting_graph_typed'):
        col = {0: 'rectangle', 1: 'triangle-up', 2: 'circle', 3: 'triangle-down'}
        width = 1000
        density = math.ceil(width / g.vcount())
        layout = g.layout('auto')
        g.vs['size'] = 10 #20
        shapes = [col[x] for x in g.vs['type']]
        g.vs['shape'] = shapes

        igraph.plot(g, self.save_directory + name + '.png',
                    edge_color='#111111', edge_width=2, iter=500, layout=layout, bbox=(0, 0, width, width))
        igraph.plot(g, self.save_directory + name + '.pdf',
                    edge_color='#111111', edge_width=2, layout=layout, bbox=(0, 0, width, width))

    def export_all(self, algorithm):
        # self.visualize_algorithm_data_summary(algorithm)
        #self.export_correlation_data(algorithm)
        self.visualize_algorithm_data_separate(algorithm)
        self.export_energy_and_prob_profile(algorithm)
        self.export_settings(algorithm)
        self.visualize_graph_structure(algorithm.system.best_proposal[0])
        algorithm.system.best_proposal[0].write_pickle(self.save_directory + 'resulting_graph.pickle')

    @staticmethod
    def get_highly_correlated_pairs(hist):
        pairs = []
        for i, j in itertools.combinations(hist.keys(), 2):
            x = zip(hist[i], hist[j])
            y = [t for t in x if abs(t[0]) > 0.0001 and abs(t[1]) > 0.0001]
            if len(y) < 2:
                continue
            a, b = zip(*y)
            correlation_coefficient = np.corrcoef(a, b)[0][1]
            if abs(correlation_coefficient) > 0.1:
                pairs.append((i, j, correlation_coefficient))
        return pairs

    def export_settings(self, algorithm):
        f = open(self.save_directory + 'run_configuration.txt', 'w')
        f.write('*SUMMARY*\n')
        f.write('fitness calls:\t%d\n' % algorithm.system.fitness.calls)
        f.write('total steps:\t%d\n\n' % algorithm.current_iteration)

        f.write('*TEMPERATURE*\n')
        f.write('initial t:\t%s\nfinal t:\t%s\ncooling rate:\t%s\nkeep temp:\t%d\n\n' %
                (str(algorithm.termometer.init_temperature), str(algorithm.termometer.low_temperature),
                 str(algorithm.termometer.cooling_rate), algorithm.termometer.keep_temp))

        f.write('*NETWORK ALTERATIONS*\n')
        f.write('adding edges:\t%d\nremoving edges:\t%d\nrewiring edges:\t%d\n\n' %
                (algorithm.system.edge_fixes, algorithm.system.edge_removals,
                 algorithm.system.edge_rewirings))

        f.write('*FITNESS VALUES*\n')
        fit = algorithm.system.fitness.print_fitness(algorithm.system.best_proposal[0])
        for k, v in fit.items():
            f.write('%s:\t%s\n' % (k, v))

        f.write('\n*NETWORK*\n')
        f.write(str(algorithm.system.best_proposal[0].summary()))
        f.close()


    def set_ax_lines_bw(self, ax):
        markersize = 3
        colormap = {
            'b': {'marker': None, 'dash': (None, None)},
            'g': {'marker': None, 'dash': [5, 5]},
            'r': {'marker': None, 'dash': [5, 3, 1, 3]},
            'c': {'marker': None, 'dash': [1, 3]},
            'm': {'marker': None, 'dash': [5, 2, 5, 2, 5, 10]},
            'y': {'marker': None, 'dash': [5, 3, 1, 2, 1, 10]},
            'k': {'marker': 'o', 'dash': (None, None)} #[1,2,1,10]}
        }

        for line in ax.get_lines():
            origcolor = line.get_color()
            line.set_color('black')
            line.set_dashes(colormap[origcolor]['dash'])
            line.set_marker(colormap[origcolor]['marker'])
            line.set_markersize(markersize)

    def set_fig_lines_bw(self, fig):
        for ax in fig.get_axes():
            self.set_ax_lines_bw(ax)
