# -*- coding: utf-8 -*-
from __future__ import division
from collections import Counter
import random
from multiprocessing import Pool
import multiprocessing

import igraph
import numpy as np

def calc_integers(current, desired, maximum=1, smoothing=0.1):
    """Calculate difference between values value >= 1.0"""
    return 1 / (smoothing * abs(current - desired) / maximum + 1)


def calc_floats(current, desired):
    """Calculate difference between values 0.0 <= value <= 1.0"""
    return 1 / (abs(current - desired) + 1)


def calc(current, desired, max_val):
    """Calculate difference between values 0.0 <= value <= 1.0"""
    return 1 - abs(current - desired) / max_val

def calc_deg(current, desired, mul=10):
    """Calculate difference between values 0.0 <= value <= 1.0"""
    return pow(2, - 0.2 * abs(current - desired) / desired)

class Fitness:
    def __init__(self, minimize=True, fit=None):
        self.fitness_set = []
        self.calls = 0
        self.minimize = minimize
        if fit:
            for f, w in fit[0].fitness_set:
                f.v = f.get_optimized_value(fit[1])
                self.fitness_set.append((f, w))

    def add(self, func_descriptor):
        if func_descriptor:
            self.fitness_set.append(func_descriptor)

    def calculate_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        self.calls += 1
        fitnesses = {}
        values = {}
        fitness = []
        for f, w in self.fitness_set:
            f_fitness, f_value = f.calculate_fitness(g)
            fitness.append(f_fitness)
            fitnesses[str(f)] = f_fitness
            values[str(f)] = f_value
        return fitness, fitnesses, values


    def print_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fit = {}
        for f, w in self.fitness_set:
            value = f.get_optimized_value(g)
            if type(value) == int:
                fit[str(f)] = str(value) + '/' + str(f.v)
            else:
                fit[str(f)] = '{0:0.12f}'.format(value) + '/' + '{0:0.12f}'.format(f.v)
        return fit

    def print_values(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fit = {}
        for f, w in self.fitness_set:
            value = f.get_optimized_value(g)
            if type(value) == int:
                fit[str(f)] = str(value)
            else:
                fit[str(f)] = '{0:0.4f}'.format(value)
        return fit




class FitnessCalculator(object):
    name = 'Noname function'

    def calculate_fitness(self, g):
        pass  # another

    def get_optimized_value(self, g):
        pass

    def __str__(self):
        return self.name


class DistributionFitnessCalculator(FitnessCalculator):
    def __init__(self, alpha):
        self.alpha = alpha

    def get_optimized_value(self, g):
        deg = np.array(g.degree()) + 1
        return igraph.power_law_fit(deg, method='auto', return_alpha_only=True)

    def calculate_fitness(self, g):
        deg = np.array(g.degree()) + 1
        al = igraph.power_law_fit(deg, method='auto', return_alpha_only=True)
        fitness = 1.0 - (abs(self.alpha - al) / max(self.alpha, al))
        return fitness

class ClusterisationFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Clusterization'

    def get_optimized_value(self, g):
        #return g.transitivity_avglocal_undirected(mode='zero')
        return g.transitivity_undirected(mode='zero')

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, 1.0), value
        #return calc_deg(value, self.v), value


class PLFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Power-Law fit'

    def get_optimized_value(self, g):
        return igraph.power_law_fit(g.degree(), return_alpha_only=False).alpha

    def calculate_fitness(self, g):
        plf = igraph.power_law_fit(g.degree(), return_alpha_only=False)
        #summ = (calc_integers(plf.alpha, self.v) + calc_floats(plf.p, 1.0)) / 2
        #summ = calc_integers(plf.alpha, self.v) * calc_floats(plf.p, 1.0)
        #summ = calc_integers(plf.alpha, self.v) * calc_floats(plf.p, 1.0) * calc_integers(plf.xmin, 1.0)
        summ = (calc_integers(plf.alpha, self.v) + calc_floats(plf.p, 1.0) + calc_integers(plf.xmin, 1.0)) / 3
        return summ, plf.alpha


class DensityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Density'
        self.max_value = 1.0

    def get_optimized_value(self, g):
        return 2 * g.ecount() / (g.vcount() * (g.vcount() - 1))

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        #return calc_deg(value, self.v), value # trick to make density better
        #return calc(value, self.v, self.max_value), value
        return calc_floats(value, self.v), value


class Motif3FitnessCalculator(FitnessCalculator):
    def __init__(self, type, v):
        self.v = v
        self.type = type
        self.name = 'Motif 3'
        self.max_value = 20.0

    def get_optimized_value(self, g):
        original_m = igraph.Graph.motifs_randesu(g, 3)[self.type]
        motifs = []
        for __ in range(30):
            g_temp = igraph.Graph.Degree_Sequence(g.degree())
            motifs.append(igraph.Graph.motifs_randesu(g_temp, 3)[self.type])

        nmotifs = np.array(motifs)
        if nmotifs.std() == 0.0:
            return 0
        return (original_m - nmotifs.mean()) / nmotifs.std()

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, self.v), value


class DanglingFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Dangling'
        self.v = 0
        pass

    def get_optimized_value(self, g):
        return len([x for x in g.degree() if x == 0])

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, 0), value


class ComponentsFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Components'
        self.v = 1
        pass

    def get_optimized_value(self, g):
        return len(g.components())

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        #return 1 / value, value
        return calc_integers(value, 1), value
        #return calc(value, 1, g.vcount()), value


class DiameterFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Diameter'

    def get_optimized_value(self, g):
        return g.diameter()

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, g.vcount()), value


class CommunityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Community'

    def get_optimized_value(self, g):
        # return g.community_walktrap().optimal_count
        return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, self.v), value


class ModularityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Modularity'

    def get_optimized_value(self, g):
        return g.community_fastgreedy().as_clustering().modularity
        # return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        #return calc(value, self.v, 1.0), value
        return calc_floats(value, self.v), value


class AssortativityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        if abs(v) > 1:
            raise Exception('Wrong assortativity value')

        self.v = v
        self.name = 'Assortativity'

    def get_optimized_value(self, g):
        return g.assortativity_degree(directed=False)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)

        if str(value) == 'nan':
            return 0, value

        return calc_floats(value, self.v), value


class TypeAssortativityFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        if abs(v) > 1:
            raise Exception('Wrong assortativity value')

        self.v = v
        self.name = 'Assortativity'

    def get_optimized_value(self, g):
        if not g.vs['type']:
            raise Exception('No type!!')
        return g.assortativity_nominal(types='type', directed=False)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)

        if str(value) == 'nan':
            return 0, value

        return calc_floats(value, self.v), value


class EfficiencyCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Efficiency'
        self.v = 1
        # self.init_value = self.calculate_efficiency(g)

    @staticmethod
    def calculate_efficiency(g):
        """Efficiency calculation"""
        result = 0.0
        paths = g.shortest_paths()
        if g.is_directed():
            for path in paths:
                for distance in path:
                    if 0 < distance < float('Inf'):
                        result += float(1) / distance
        else:
            for index, path in enumerate(paths):
                for distance in path[index + 1:]:
                    if 0 < distance < float('Inf'):
                        result += float(2) / distance

        n = g.vcount()
        result /= n * (n - 1)
        return result

    def get_optimized_value(self, g):
        return self.calculate_efficiency(g)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_floats(value, 1.0), value


class ShortestPathsCalculator(FitnessCalculator):
    def __init__(self, v):
        self.name = 'Shortest paths'
        self.v = v
        # self.init_value = self.calculate_efficiency(g)

    def get_optimized_value(self, g):
        return g.average_path_length(unconn=False)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_integers(value, self.v), value


class DegreeSequenceFitCalculator(FitnessCalculator):
    def __init__(self, sequence):
        self.sequence = sequence
        self.c_desired = Counter(self.sequence)
        self.sequence.sort()
        self.maximum = 1
        self.name = 'Degree sequence'
        self.v = 0.0
        # self.init_value = self.calculate_efficiency(g)

    # def calculate_sequence_fit(self, g):
    #     if g.vcount() != len(self.sequence):
    #         raise Exception('Sequence length is not equal nodes count')
    #     present_sequence = g.degree()
    #     present_sequence.sort()
    #     return sum(map(operator.abs, map(operator.sub, present_sequence, self.sequence)))

    def calculate_sequence_fit(self, g):
        if g.vcount() != len(self.sequence):
            raise Exception('Sequence length is not equal nodes count')
        present_sequence = g.degree()
        c_degrees = Counter(present_sequence)
        c_degrees.subtract(self.c_desired)
        return sum([abs(x) for x in c_degrees.values()]) / (g.vcount() * 2.0)

    def get_optimized_value(self, g):
        return self.calculate_sequence_fit(g)

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        if self.maximum < abs(value - 0):
            self.maximum = abs(value - 0)

        # div = max(curr_m, self.init_value)
        # fitness = abs(1 - abs(curr_m - self.m) / div)
        return calc(value, 0, 1.0), value
        #return calc_floats(value, 0), value


class MeanDegreeFitnessCalculator(FitnessCalculator):
    def __init__(self, v):
        self.v = v
        self.name = 'Mean Degree'

    def get_optimized_value(self, g):
        return np.array(g.degree()).mean()

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc(value, self.v, g.vcount() - 1), value


class SirFitnessCalculator2(FitnessCalculator):
    def __init__(self, v, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.v = v
        self.name = 'Sir percentage of R'
        self.nei = None

    def make_sir(self, g_or):
        g = g_or.copy()
        pop_size = g.vcount()
        i_list = set()
        r_list = set()
        s_list = set(range(pop_size))
        f = random.randint(0, pop_size - 1)
        # f = 1

        i_list.add(f)
        s_list.remove(f)

        if not self.nei:
            for k in range(pop_size):
                self.nei[k] = set(g.neighbors(g.vs[k]))

        while len(i_list) > 0:

            newI = set()
            newR = set()
            tempS = set()

            for i in i_list:
                tempS |= self.nei[i]
            tempS -= r_list
            tempS -= i_list
            for s in tempS:
                if random.random() < self.alpha:
                    newI.add(s)
            for i in i_list:
                if random.random() < self.beta:
                    newR.add(i)

            s_list -= newI
            i_list -= newR
            i_list |= newI
            r_list |= newR
        return float(len(r_list)) / pop_size

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_floats(value, self.v), value

    def get_optimized_value(self, g):
        neig = g.neighbors

        self.nei = dict()
        for k in range(g.vcount()):
            self.nei[k] = set(neig(g.vs[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g)
            if l > 1.0 / g.vcount():
                result.append(l)

        # return np.array(result).mean(), result
        if result:
            return np.array(result).mean()
        else:
            return 0.0


# ------------------------------------------------------------------
def make_sir((g, nei, alpha, beta)):
    pop_size = g.vcount()
    i_list = set()
    r_list = set()
    s_list = set(range(pop_size))
    # f = random.randint(0, pop_size - 1)
    f = 1

    i_list.add(f)
    s_list.remove(f)

    while len(i_list) > 0:
        newI = set()
        newR = set()
        tempS = set()

        for i in i_list:
            tempS |= nei[i]
        tempS -= r_list
        tempS -= i_list
        for s in tempS:
            if random.random() < alpha:
                newI.add(s)
        for i in i_list:
            if random.random() < beta:
                newR.add(i)

        s_list -= newI
        i_list -= newR
        i_list |= newI
        r_list |= newR
    return float(len(r_list)) / pop_size

class SirFitnessCalculator(FitnessCalculator):
    def __init__(self, v, alpha=0.3, beta=0.5, runs=50):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.v = v
        self.name = 'Sir percentage of R'
        self.nei = None


    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        return calc_floats(value, self.v), value

    def get_optimized_value(self, g):
        neig = g.neighbors

        self.nei = dict()
        for k in range(g.vcount()):
            self.nei[k] = set(neig(g.vs[k]))

        pool_size = 4
        pool = Pool(processes=pool_size)
        result = pool.map(make_sir, [(g, self.nei, self.alpha, self.beta) for i in range(self.runs)])
        pool.close()
        pool.join()

        tres = 1.0 / g.vcount()
        result = filter(lambda x: x > tres, result)
        if result:
            return np.array(result).mean()
        else:
            return 0.0


# ------------------------------------------------------------------


class SirDaysFitnessCalculator(FitnessCalculator):
    def __init__(self, v, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.v = v
        self.name = 'Sir days'

    def make_sir(self, g_or, nei=None):
        g = g_or.copy()
        pop_size = g.vcount()
        iList = set()
        rList = set()
        sList = set(range(pop_size))
        f = random.randint(0, pop_size - 1)
        #f = 1

        iList.add(f)
        sList.remove(f)
        iterations = 0

        if not nei:
            for k in range(pop_size):
                nei[k] = set(g.neighbors(g.vs[k]))

        while len(iList) > 0:
            iterations += 1
            new_i = set()
            new_r = set()
            temp_s = set()

            for i in iList:
                temp_s |= nei[i]
            temp_s -= rList
            temp_s -= iList
            for s in temp_s:
                if random.random() < self.alpha:
                    new_i.add(s)
            for i in iList:
                if random.random() < self.beta:
                    new_r.add(i)

            sList -= new_i
            iList -= new_r
            iList |= new_i
            rList |= new_r
        return iterations

    def calculate_fitness(self, g):
        value = self.get_optimized_value(g)
        if abs(value - self.v) < 0.3:
            value = self.get_optimized_value(g)
            if abs(value - self.v) < 0.3:
                return 1, value
            else:
                return calc_integers(value, self.v), value
        else:
            return calc_integers(value, self.v), value

    def get_optimized_value(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)

            # Attention!
            if l > 1.0:
                result.append(l)

        return np.array(result).mean() #, np.array(result).std()


assortativity_fit = AssortativityFitnessCalculator(v=-0.2)
type_assortativity_fit = TypeAssortativityFitnessCalculator(v=-0.2)
clusterisation_fit = ClusterisationFitnessCalculator(v=0.01)
community_fit = CommunityFitnessCalculator(v=3)
components_fit = ComponentsFitnessCalculator()
dangling_fit = DanglingFitnessCalculator()
degree_sequence_fit = DegreeSequenceFitCalculator(sequence=igraph.Graph.Barabasi(300, 1).degree())
density_fit = DensityFitnessCalculator(v=0.03)  # minimal density for connected graph = 2 / g.vcount()
diameter_fit = DiameterFitnessCalculator(v=7)
distribution_fit = DistributionFitnessCalculator(alpha=2.5)
eff_fit = EfficiencyCalculator()
mean_degree_fit = MeanDegreeFitnessCalculator(v=3)  # mean degree == density*(N-1)
modularity_fit = ModularityFitnessCalculator(v=0.9)
pl_fit = PLFitnessCalculator(v=3)
sir_days_fit = SirDaysFitnessCalculator(v=10, runs=100)
sir_fit = SirFitnessCalculator(v=0.5, runs=300)
motif_fit = Motif3FitnessCalculator(type=3, v=10.0)
short_path_fit = ShortestPathsCalculator(4)


superFitness = Fitness()
superFitness.add((assortativity_fit, 1))
superFitness.add((clusterisation_fit, 1))
superFitness.add((community_fit, 0.5))
superFitness.add((mean_degree_fit, 1))
superFitness.add((modularity_fit, 0.5))
superFitness.add((components_fit, 2))
superFitness.add((eff_fit, 1))
superFitness.add((short_path_fit, 1))
superFitness.add((density_fit, 3))
superFitness.add((pl_fit, 1))