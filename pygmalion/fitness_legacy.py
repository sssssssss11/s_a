from __future__ import division
from collections import Counter
import random
import math

import igraph
import numpy as np


def zeta(s):
    summary = 0
    for i in range(1, 100):
        summary += 1 / (i ** s)
    return s

zet = {}
for x in range(1, 300):
    zet[x] = zeta(x)


def power_law(k, a):
    return pow(k, -a) / zeta(a)


def get_probabilities(n=100, alpha=2):
    probabilities = [0]
    for k in range(1, 200 + 1):
        probabilities.append(power_law(k, alpha))
    return probabilities


def get_net_probs(g):
    c = Counter(g.degree())
    probs = []
    max_key = max(c.keys())
    for k in range(0, max_key + 1):
        probs.append(c[k] / g.vcount())
    return probs


class Fitness:
    def __init__(self):
        self.fitness_set = []

    def add(self, func_descriptor):
        if func_descriptor:
            self.fitness_set.append(func_descriptor)

    def calculate_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fitness_dic = {}
        fitness = 0.0
        total_w = 0
        for f, w in self.fitness_set:
            fit = f.calculate_fitness(g)
            fitness += w * fit
            total_w += w
            fitness_dic[str(f)] = fit
        fitness /= total_w
        return 1 - fitness, fitness_dic

    def print_fitness(self, g):
        if len(self.fitness_set) == 0:
            raise Exception('Fitness set is empty or graph is not present!')
        fit = {}
        for f, w in self.fitness_set:
            value = f.get_optimized_value(g)
            if type(value) == int:
                fit[str(f)] = str(value)
            else:
                fit[str(f)] = '{0:0.4f}'.format(value)
        return fit


class FitnessCalculator(object):
    name = 'Noname function'

    def calculate_fitness(self, g):
        pass  # another

    def get_optimized_value(self, g):
        pass

    def __str__(self):
        return self.name


class DistributionFitnessCalculator(FitnessCalculator):
    def __init__(self, alpha):
        self.alpha = alpha

    def get_optimized_value(self, g):
        deg = np.array(g.degree()) + 1
        return igraph.power_law_fit(deg, method='auto', return_alpha_only=True)

    def calculate_fitness(self, g):
        deg = np.array(g.degree()) + 1
        al = igraph.power_law_fit(deg, method='auto', return_alpha_only=True)
        fitness = 1.0 - (abs(self.alpha - al) / max(self.alpha, al))
        return fitness


class PowerlawFitnessCalculator(FitnessCalculator):
    def __init__(self, alpha):
        self.alpha = alpha

    def calculate_fitness(self, g):
        s2 = get_net_probs(g)
        high_cut = -1
        for i, d in enumerate(reversed(s2)):
            if d > 0:
                high_cut = i
                break

        if high_cut > 0:
            s2 = s2[0:-high_cut]

        s1 = get_probabilities(g.vcount(), self.alpha)
        s1 = s1[0:len(s2)]

        l = 0
        for x, y in zip(s1, s2):
            l += (x - y) ** 2
        l /= len(s2)
        l = abs(1 - math.sqrt(l))

        return l


class ClusterisationFitnessCalculator(FitnessCalculator):
    def __init__(self, c):
        self.c = c
        self.name = 'Clusterization'

    def get_optimized_value(self, g):
        return g.transitivity_avglocal_undirected(mode='zero')

    def calculate_fitness(self, g):
        trans = self.get_optimized_value(g)
        fitness = 1.0 - abs(self.c - trans)
        return fitness


class DensityFitnessCalculator(FitnessCalculator):
    def __init__(self, d):
        self.d = d
        self.name = 'Density'

    def get_optimized_value(self, g):
        return 2 * g.ecount() / (g.vcount() * (g.vcount() - 1))

    def calculate_fitness(self, g):
        density = self.get_optimized_value(g)
        fitness = 1.0 - abs(self.d - density)
        return fitness


class DanglingFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Dangling nodes'
        pass

    def get_optimized_value(self, g):
        return len([x for x in g.degree() if x == 0])

    def calculate_fitness(self, g):
        dangling_nodes_count = self.get_optimized_value(g)
        fitness = (g.vcount() - dangling_nodes_count) / g.vcount()
        # fitness = 1 - abs(0 - dangling_nodes_count) / g.vcount()
        return fitness


class ComponentsFitnessCalculator(FitnessCalculator):
    def __init__(self):
        self.name = 'Components'
        pass

    def get_optimized_value(self, g):
        return len(g.components())

    def calculate_fitness(self, g):
        n_components = self.get_optimized_value(g)
        fitness = 1 / n_components
        return fitness


class DiameterFitnessCalculator(FitnessCalculator):
    def __init__(self, d):
        self.d = d
        self.name = 'Diameter'

    def get_optimized_value(self, g):
        return g.diameter()

    def calculate_fitness(self, g):
        curr_d = self.get_optimized_value(g)
        fitness = abs(1 - abs(curr_d - self.d) / max(curr_d, self.d))
        return fitness


class CommunityFitnessCalculator(FitnessCalculator):
    def __init__(self, c):
        self.c = c
        self.name = 'Community'

    def get_optimized_value(self, g):
        # return g.community_walktrap().optimal_count
        return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        curr_c = self.get_optimized_value(g)
        div = max(curr_c, self.c)
        fitness = abs(1 - abs(curr_c - self.c) / div)
        return fitness


class ModularityFitnessCalculator(FitnessCalculator):
    def __init__(self, m):
        self.m = m
        self.name = 'Modularity'

    def get_optimized_value(self, g):
        return g.community_fastgreedy().as_clustering().modularity
        # return g.community_fastgreedy().optimal_count
        # return g.community_edge_betweenness(directed=False).optimal_count

    def calculate_fitness(self, g):
        curr_m = self.get_optimized_value(g)
        div = max(curr_m, self.m)
        fitness = abs(1 - abs(curr_m - self.m) / div)
        return fitness


class EfficiencyCalculator():
    def __init__(self):
        self.name = 'Efficiency'
        # self.init_value = self.calculate_efficiency(g)

    def calculate_efficiency(self, g):
        shortest_paths = g.shortest_paths()
        eff = 0
        for i in range(0, g.vcount() - 2):
            for j in range(i + 1, g.vcount()):
                eff += 1 / shortest_paths[i][j]

        eff /= g.vcount() * (g.vcount() - 1)
        return eff

    def get_optimized_value(self, g):
        return self.calculate_efficiency(g)

    def calculate_fitness(self, g):
        curr_m = self.get_optimized_value(g)
        # div = max(curr_m, self.init_value)
        # fitness = abs(1 - abs(curr_m - self.m) / div)
        return curr_m


class MeanDegreeFitnessCalculator(FitnessCalculator):
    def __init__(self, d):
        self.d = d
        self.name = 'Mean Degree'

    def get_optimized_value(self, g):
        return np.array(g.degree()).mean()

    def calculate_fitness(self, g):
        curr_mean_d = self.get_optimized_value(g)
        if curr_mean_d == 0:
            return 0
        div = max(curr_mean_d, self.d)
        fitness = abs(1 - abs(curr_mean_d - self.d) / div)
        return fitness


class SirFitnessCalculator(FitnessCalculator):
    def __init__(self, p, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.p = p
        self.name = 'Sir percentage'

    def make_sir(self, g_or, nei=None):
        g = g_or.copy()
        pop_size = g.vcount()
        iList = set()
        rList = set()
        sList = set(range(pop_size))
        f = random.randint(0, pop_size - 1)
        # f = 1

        iList.add(f)
        sList.remove(f)

        if not nei:
            for k in range(pop_size):
                nei[k] = set(g.neighbors(g.vs[k]))

        while len(iList) > 0:

            newI = set()
            newR = set()
            tempS = set()

            for i in iList:
                tempS |= nei[i]
            tempS -= rList
            tempS -= iList
            for s in tempS:
                if random.random() < self.alpha:
                    newI.add(s)
            for i in iList:
                if random.random() < self.beta:
                    newR.add(i)

            sList -= newI
            iList -= newR
            iList |= newI
            rList |= newR
        return float(len(rList)) / pop_size

    def calculate_fitness(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        return 1 - abs(np.array(result).mean() - self.p)

    def get_optimized_value(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        return 1 - np.array(result).mean()


class SirDaysFitnessCalculator(FitnessCalculator):
    def __init__(self, days, alpha=0.3, beta=0.5, runs=10):
        self.alpha = alpha
        self.beta = beta
        self.runs = runs
        self.days = days
        self.name = 'Sir days'

    def make_sir(self, g_or, nei=None):
        g = g_or.copy()
        pop_size = g.vcount()
        iList = set()
        rList = set()
        sList = set(range(pop_size))
        # f = random.randint(0, pop_size - 1)
        f = 1

        iList.add(f)
        sList.remove(f)
        iterations = 0

        if not nei:
            for k in range(pop_size):
                nei[k] = set(g.neighbors(g.vs[k]))

        while len(iList) > 0:
            iterations += 1
            newI = set()
            newR = set()
            tempS = set()

            for i in iList:
                tempS |= nei[i]
            tempS -= rList
            tempS -= iList
            for s in tempS:
                if random.random() < self.alpha:
                    newI.add(s)
            for i in iList:
                if random.random() < self.beta:
                    newR.add(i)

            sList -= newI
            iList -= newR
            iList |= newI
            rList |= newR
        return iterations

    def calculate_fitness(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        total_days = np.array(result).mean()
        if abs(total_days - self.days) < 2:
            return 1
        else:
            return 1 - abs(total_days - self.days) / max(total_days, self.days)

    def get_optimized_value(self, g):
        nei = dict()
        neig = g.neighbors

        v = g.vs
        for k in range(g.vcount()):
            nei[k] = set(neig(v[k]))

        result = []
        for n in range(self.runs):
            l = self.make_sir(g, nei=nei)
            result.append(l)

        return np.array(result).mean()

