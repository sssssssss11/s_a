*SUMMARY*
fitness calls:	3004
total steps:	3003

*TEMPERATURE*
initial t:	100
final t:	0.1
cooling rate:	0.999
keep temp:	3

*NETWORK ALTERATIONS*
adding edges:	10
removing edges:	10
rewiring edges:	10

*FITNESS VALUES*
Clusterization:	0.0250/0.0480
Modularity:	0.2686/0.9000
Density:	0.0235/0.0100

*NETWORK*
IGRAPH U--- 500 2930 -- 
+ attr: generation (g)