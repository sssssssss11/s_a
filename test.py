import numpy as np
import csv
import igraph
import matplotlib.pyplot as plt
from math import log
import itertools
import pandas as pd

def get_bankGraph(method_name):
    path = method_name+".graphml"
    result = igraph.Graph.Read_GraphML(path)
    return result

#Forming array from proposal element, parametr arr must be a proposal array that can be found from simulated anealing,
#parametr type must be chose from 2: "Clasterizatiom", "Density"

def double_viz(arr1, arr2, label1, label2, arr3=[],label3=''):
    edge_arr1 = [i[0] for i in arr1]
    iter_arr1 = [i[1] for i in arr1]
    edge_arr2 = [i[0] for i in arr2]
    iter_arr2 = [i[1] for i in arr2]
    edge_arr3 = [i[0] for i in arr3]
    iter_arr3 = [i[1] for i in arr3]
    ax = plt.subplot()
    ax.plot(iter_arr1, edge_arr1, 'r', label=label1, linewidth=2.0)
    ax.plot(iter_arr2, edge_arr2, 'g',   label=label2, linewidth=2.0)
    ax.plot(iter_arr3, edge_arr3, 'b', label=label3, linewidth=2.0)

    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontsize(16)

    if label1 == 'Added':
        plt.ylabel("Edge count", fontsize=16)
        plt.legend(loc=2, prop={'size':18})
        plt.ylim(0, 60000)
    else:
        plt.ylim(-0.01, 0.1)
        plt.legend(loc=5, prop={'size':18})

    plt.xlabel("Iterations", fontsize=16)
    plt.show()



def cum_arr(arr):
    acc = 0
    new_arr =[]
    for i in arr:
        new_arr.append((i[0]+acc,i[1]))
        acc += i[0]
    return new_arr

def form_arr(arr, type=''):
    res = []
    acc = arr[0][0]
    for i in arr:
        if type == 'add':
            res.append((i[0]-acc+100,i[1]))
        elif type =='total':
            res.append((i[0] - acc, i[1]))
        acc = i[0]
    return res

def delE_arr(arr, k):
    return [(k,i[1]) for i in arr]

def line_split(field):
    for line in field:
        yield line.split(' ')

def read_arr(file_name):
    with open(file_name+'.txt', 'r') as f:
        result = [
            (eval(first), eval(second)) for first, second in line_split(f)
            ]
    f.close()
    return result


def hlop_arr(val, res = []):
    for i in val:
        if isinstance(i,list):
            hlop_arr(i)
        else:
            res.append(i)

    return res


def coll(arr):
    for i in range(len(arr)):
        yield arr[i]

def vloj(arr):
    return [(f, b) for f, b in itertools.izip(coll(arr), coll(arr))]

def diff():
    pd.read_csv()

def a():
    return 1, 2, 3

if __name__ == '__main__':
    #clust_arr = read_arr('CLUST_ARR')
    #dens_arr = read_arr('DENS_ARR')
    #edge_arr = read_arr('EDGE_ARR')
    #energy_arr = read_arr('ENERGY_ARR')
#
    #double_viz(clust_arr, energy_arr, 'Clustering', 'Energy')
    #double_viz(cum_arr(form_arr(edge_arr,type='add')),
    #           cum_arr(delE_arr(edge_arr, 100)), 'Added', 'Deleted',
    #           arr3=cum_arr(form_arr(edge_arr,type='total')), label3='Total')
#
    #arr = [1,2,3,4,5]
    #print vloj(arr)
    print a()