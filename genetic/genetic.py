from __future__ import division
import random
import operator
from collections import defaultdict
import itertools
import numpy as np
import copy
import igraph
from pygmalion.pygmalion import Pygmalion


class DismantleGraph(object):
    def __init__(self, n, limit=300, size=50,
                 prob_crossover=0.9, prob_mutation=0.2, k=100, l=500, fit=None, verbose=True):
        self.n = n
        self.k = k
        self.l = l
        self.counter = 0
        self.iterations_limit = limit
        self.size = size
        self.fit = fit
        self.prob_crossover = prob_crossover
        self.prob_mutation = prob_mutation
        self.verbose = verbose
        self._edge_pairs = list(itertools.combinations(list(range(0, n)), 2))
        self.pygmalion = Pygmalion(igraph.Graph(n))
        self.log_keeper = defaultdict(lambda: list())
        pass

    def probability_crossover(self):
        return self.prob_crossover

    def probability_mutation(self):
        return self.prob_mutation

    def initial(self):
        return [self.random_chromo() for j in range(self.size)]

    def fitness(self, chromo):
        return self.fit.calculate_fitness(chromo)

    def check_stop(self, fits_populations):
        self.counter += 1
        to_stop_by_epsilon = False
        if self.counter % 1 == 0:
            best_match = list(sorted(fits_populations, key=lambda l: l[0][0]))[-1][0]
            fits = [f[0] for f, ch in fits_populations]
            best = max(fits)
            worst = min(fits)
            ave = sum(fits) / len(fits)
            if self.verbose:
                print "[G %3d] score=(%0.3f, %0.3f, %0.3f): %s" % (self.counter, best, ave, worst, str(best_match))
            if 1.0 - best < 0.002:
                to_stop_by_epsilon = True
            pass
        return self.counter >= self.iterations_limit or to_stop_by_epsilon

    def parents(self, fits_populations):
        while True:
            father = self.tournament(fits_populations)
            mother = self.tournament(fits_populations)
            yield (father, mother)
            pass
        pass

    def crossover(self, parents):
        g1, g2 = parents
        edges1 = list(g1.get_edgelist())
        edges2 = list(g2.get_edgelist())

        summ = edges1 + edges2
        summ = sorted(summ)
        edges1 = summ[0::2]
        edges2 = summ[1::2]

        return igraph.Graph(edges1), igraph.Graph(edges2)

    # def crossover(self, parents):
    #     father, mother = parents
    #     for x in range(int(father.vcount())):
    #         x1, x2 = random.randint(0, father.vcount() - 1), random.randint(0, father.vcount() - 1)
    #         tmp = mother[x1, x2]
    #         mother[x1, x2] = father[x1, x2]
    #         father[x1, x2] = tmp
    #     return father, mother

    def mutation(self, chromosome):
        proposal_g = chromosome
        random_value = random.random()
        # times = 5
        times = random.randint(1, 20)
        if random_value <= 1 / 3:
            # proposal_g.add_edges(random.sample(list(itertools.combinations(list(range(0, proposal_g.vcount())), 2)), times))
            self.pygmalion.add_edge_simple(proposal_g, times=times, shift=False)
        elif 1 / 3 < random_value <= 2 / 3:
            self.pygmalion.remove_random_edge(proposal_g, times=times, shift=False)
        else:
            self.pygmalion.rewire_random_edges(proposal_g, times=times, shift=False)
        return proposal_g

    # internals
    def tournament(self, fits_populations):
        a = self.select_random(fits_populations)
        b = self.select_random(fits_populations)
        c = self.select_random(fits_populations)
        d = self.select_random(fits_populations)
        return max(a, b, c, d, key=operator.itemgetter(0))[1]

    def select_random(self, fits_populations):
        return fits_populations[random.randint(0, len(fits_populations) - 1)]

    def random_chromo(self):
        g = igraph.Graph.Erdos_Renyi(n=self.n, m=random.randint(0, self.n * (self.n - 1) / 5))
        return g


class GeneticAlgorithm(object):
    def __init__(self, genetics):
        self.genetics = genetics
        self.best_proposal = (None, -1)

    def search_run(self):
        population = self.genetics.initial()
        best = []
        print 'Calculation begin'
        while True:
            fits_pops = [(self.genetics.fitness(ch), ch) for ch in population]
            b = list(sorted(fits_pops, key=lambda l: l[0][0]))[-1]
            #best.append(copy.deepcopy(b))
            if self.best_proposal[1] == -1 or b[0][0] > self.best_proposal[1]:
                self.best_proposal = b, b[0][0]

            if self.genetics.check_stop(fits_pops):
                #return list(sorted(best, key=lambda l: l[0]))[-1]
                return self.best_proposal
            population = self.next_generation(fits_pops)


    def next_generation(self, fits):
        parents_generator = self.genetics.parents(fits)
        size = len(fits)
        n_best = int(0.1 * size)
        bests = list(sorted(fits, key=lambda l: l[0][0]))[-n_best:]
        nexts = [b[1] for b in bests]
        while len(nexts) < size:
            parents = next(parents_generator)
            cross = random.random() < self.genetics.probability_crossover()
            children = self.genetics.crossover(parents) if cross else parents
            for ch in children:
                mutate = random.random() < self.genetics.probability_mutation()
                #mutate = True
                nexts.append(self.genetics.mutation(ch) if mutate else ch)
        return nexts[0:size]
