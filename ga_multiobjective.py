# -*- coding: utf-8 -*-
import datetime
import os
import pickle
import igraph
from genetic import ga_utilities

from genetic.genetic33 import *
from pygmalion.fitness_multiobjective import *

fitness = Fitness(minimize=False)

# fitness.add((DensityFitnessCalculator(0.1), 1))
# fitness.add((ClusterisationFitnessCalculator(1.0), 1))
# fitness.add((ShortestPathsCalculator(1), 1))

g = igraph.Graph.Read_Ncol('tests for thesis/nbp/airport.txt')
g.to_undirected()
g.simplify()
g.delete_vertices(g.vs.select(_degree=0))
print g.summary()

v1 = SirFitnessCalculator(0.5, 0.5, 0.9, 100).get_optimized_value(g)
print v1
v2 = SirFitnessCalculator(0.5, 0.2, 0.3, 100).get_optimized_value(g)
print v2

f1 = SirFitnessCalculator(v1, 0.5, 0.9, 100)
f1.name = 'Sir 1'
f2 = SirFitnessCalculator(v2, 0.2, 0.3, 100)
f2.name = 'Sir 2'
fitness.add((f1, 1))
fitness.add((f2, 1))
# fitness.add((DensityFitnessCalculator(0.023), 1))
# fitness.add(SirFitnessCalculator())
# fitness.add((components_fit, 1))


def run_program():
    save_directory_name = 'simulations/ga_' + datetime.datetime.today().strftime("%d.%m.%y %H:%M:%S") + '/'
    os.mkdir(save_directory_name)
    n = 500
    genetics = DismantleGraph(n, limit=20, size=50, prob_crossover=0.5, prob_mutation=0.2, fit=fitness, verbose=True)

    ga = GeneticAlgorithm(genetics)

    try:
        population = ga.search_run()
        for g in population:
            # print fitness.print_fitness(g[0])
            print fitness.print_values(g[0])
        # print 'Best strategy: ' + str(genetic_best)

        from matplotlib import pyplot as plt

        plt.scatter([x[1][0] for x in population], [x[1][1] for x in population], c='r')
        pops = nsga2(population)
        plt.scatter([x[1][0] for x in nsga2(population)], [x[1][1] for x in pops], c='b')
        plt.xlabel('Clusterisation')
        plt.ylabel('Shortest path length')
        plt.show()
        for s in pops:
            v1 = s[1][0]
            v2 = s[1][1]
            g = s[0]
            igraph.Graph.write_pickle(g, 'tests for thesis/nbp/solutions/%s_%s.pickle' % (str(v1), str(v2)))


    except KeyboardInterrupt:
        pass
    finally:
        print 'Fitness calls N: ', ga.genetics.fit.calls
        # exporter = ga_utilities.Exporter(save_directory_name)
        # exporter.export_all(ga)


if __name__ == '__main__':
    run_program()
