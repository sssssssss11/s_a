# -*- coding: utf-8 -*-
from __future__ import division
import datetime
import os
import igraphplus.metrics
from pygmalion.annealing import *
from pygmalion.fitness_with_scale import *
from pygmalion import utilities

assortativity_fit = AssortativityFitnessCalculator(v=-0.2)
clusterisation_fit = ClusterisationFitnessCalculator(v=0.1)
community_fit = CommunityFitnessCalculator(v=3)
components_fit = ComponentsFitnessCalculator()
dangling_fit = DanglingFitnessCalculator()
degree_sequence_fit = DegreeSequenceFitCalculator(sequence=igraph.Graph.Barabasi(300, 1).degree())
density_fit = DensityFitnessCalculator(v=0.008)  # minimal density for connected graph = 2 / g.vcount()
diameter_fit = DiameterFitnessCalculator(v=7)
distribution_fit = DistributionFitnessCalculator(alpha=2.5)
eff_fit = EfficiencyCalculator()
mean_degree_fit = MeanDegreeFitnessCalculator(v=3)  # mean degree == density*(N-1)
modularity_fit = ModularityFitnessCalculator(v=0.9)
sir_days_fit = SirDaysFitnessCalculator(v=15, runs=200)
sir_fit = SirFitnessCalculator(v=0.5, runs=300)

fitness = Fitness()
#fitness.add((assortativity_fit, 1))
#fitness.add((clusterisation_fit, 1))
#fitness.add((community_fit, 1))
#fitness.add((mean_degree_fit, 1))
#fitness.add((modularity_fit, 1))
fitness.add((sir_days_fit, 1))
fitness.add((components_fit, 2))
#fitness.add((eff_fit, 1))
#fitness.add((degree_sequence_fit, 1))
fitness.add((density_fit, 1))


def run_program():
    save_directory_name = 'simulations/' + datetime.datetime.today().strftime("%d.%m.%y %H:%M:%S") + '/'
    os.mkdir(save_directory_name)

    g = igraph.Graph(300)

    system = NetworkModifier(g, fitness, edge_fixes=3, edge_removals=3, edge_rewirings=10)

    termometer = TemperatureKeeper(high_temperature=100, low_temperature=1, cooling_rate=0.999, keep_temperature=1)

    algorithm = SimulatedAnnealing(termometer, system)
    try:
        g, fit = algorithm.run_annealing()
        #print g.summary()
        print 'finishing'
        print fit, fitness.print_fitness(g)
    except KeyboardInterrupt:
        pass
    #except TypeError:
    #    print 'Error with type'
    finally:
        print algorithm.system.best_proposal[0].summary()
        exporter = utilities.Exporter(save_directory_name)
        #exporter.visualize_correlation(algorithm)
        #exporter.visualize_actions(algorithm)
        #exporter.visualize_actions_success(algorithm)
        #exporter.visualize_fitnesses(algorithm)
        exporter.export_all(algorithm)
        #print g.summary()
        print igraphplus.metrics.summary(g)
        pl_cum(g)
        #print igraphplus.metrics.degree_distribution_plot(g, loglog=True)


def pl_cum(g):
    print g.summary()
    degrees = Counter(g.degree())
    freq = np.array(degrees.values())
    freq = freq / g.vcount()
    print freq
    cumfreq = [sum(freq[i:]) for i in range(len(freq))]
    print cumfreq
    import pylab
    pylab.clf()
    pylab.xscale('log')
    pylab.yscale('log')
    pylab.xlabel('k')
    pylab.ylabel('N')
    pylab.title('Degree distribution')
    pylab.plot(range(len(cumfreq)), cumfreq, '*')
    pylab.show()

if __name__ == '__main__':
    run_program()
